# README #

### Draw.io Library for Genograms ###

* This library for draw.io includes vector based icons for genogram generation.
* Version 0.5.1

Direct link to test this library: [Genogram Library](https://www.draw.io/?splash=0&clibs=Uhttps%3A%2F%2Fbitbucket.org%2Flbigalke%2Fdraw.io-genogram-lib%2Fraw%2Fmaster%2Flibrary.xml)
